Changing the selected region in the management console will not affect the listed instances in your account.
Answer: NO

What is the meaning of PEM in a PEM file?
Answer: Privacy Enhanced Mail 

How many instance types does Amazon EC2 service have?
Answer: 5

AWS stands for?
Answer: Amazon Web Services

VM stands for?
Answer: Virtual Machine

What is the name of the instance type we used to create a new instance?
Answer: General Purpose Instance/ t2.micro

Refers to on-demand availability of computing-related resources such as servers, storage and databases without direct hardware management by the user.
Answer: Cloud computing

Contains a variety of services that can be used together in order to quickly build and deploy applications.
Answer: Cloud Computing Platform

Refers to a service (specifically, Platform as a Service or PaaS) offered to businesses both large and small.
Answer: Cloud Computing Platform

What is an alternative term for virtual machine within your own machine?
Answer: Local virtual machine

It is a term used within AWS to refer to virtual machines.
Answer: EC2

How many total regions do AWS offer in its EC2 service?
Answer: 31


--USE AWS EC2 CALCULATOR FOR THIS PART--
LINK: calculator.aws/#/createCalculator/EC2
OBJECTIVE: Get the monthly pricing for the specified specifications.

Osaka region, Windows Server operating system, 2 vCPUs, 8 GiB memory
Instance type name: t3a.large
Monthly pricing: 2,199.64 USD

Ohio region, Linux operating system, 2 vCPUs, 8 GiB memory
Instance type name: t4g.large
Monthly pricing: $796.28

Stockholm region, Linux with SQL Server Web operating system, 4 vCPUs, 16 GiB memory
Instance type name: t4g.xlarge
Monthly pricing: t4g.xlarge not available on choices

Singapore region, SUSE Linux Enterprise Server operating system, 8 vCPUs, 32 GiB memory
Instance type name: t4g.2xlarge
Monthly pricing: 5,358.49 USD 





